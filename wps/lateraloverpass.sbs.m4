# Font declarations.
%Fl(2, _FONT2)
%Fl(3, _FONT3)
%Fl(4, _FONT4)
%Fl(5, _FONT5)
%Fl(6, _FONT6)
%Fl(7, _FONT7)
%Fl(8, _FONT8)

# No background image.
%X(d)

# Custom UI Viewport.
%Vi(-, 0, 0, 198, 320, 6)%Vb(_COLORBG)

# Image declarations.
%xl(A, playstate.bmp,  0, 0, 5)
%xl(B, icons.bmp,      0, 0, 4)
%xl(C, power-lock.bmp, 0, 0, 4)

# Right side, clock.
%V(198, 0, 42, 20, 5)%Vf(_COLOR3)%Vb(_COLORBG)
ifelse(_SIMULATOR, `1', `%ar22:08', `%ar%ck:%cM')

# Right side, date.
%V(198, 21, 42, 60, 8)%Vf(_COLOR3)%Vb(_COLORBG)
ifelse(_SIMULATOR, `1', `%arWed', `%ar%ca')
ifelse(_SIMULATOR, `1', `%arDec', `%ar%cb')
ifelse(_SIMULATOR, `1', `%ar23',  `%ar%ce')

# Right side, battery bar.
%V(201, 69, 18, 39, -)%Vf(_COLOR2)%Vb(_COLORBG)
%dr(0, 0, 18, 39, _COLOR1)
%dr(1, 1, 16, 37, _COLORBG)
%bl(3, 3, 12, 33, -, vertical)

# Right side, volume bar.
%V(222, 69, 18, 39, -)%Vf(_COLOR2)%Vb(_COLORBG)
%dr(0, 0, 18, 39, _COLOR1)
%dr(1, 1, 16, 37, _COLORBG)
%pv(3, 3, 12, 33, -, vertical)

# Right side, battery value.
%V(201, 113, 18, 24, 2)%Vf(_COLOR2)%Vb(_COLORBG)
%ac%bl
%ac%%

# Right side, volume value.
%V(222, 113, 18, 24, 2)%Vf(_COLOR2)%Vb(_COLORBG)
%ac%ss(0, 3, %pv)
%acdB

# Right side, random shuffle icon.
%V(201, 147, 18, 18, -)%Vf(_COLOR1)%Vb(_COLORBG)
%?ps<%xd(Bb)|%xd(Ba)>

# Right side, repeat icon.
%V(222, 147, 18, 18, -)%Vf(_COLOR1)%Vb(_COLORBG)
%?mm<%xd(Bc)|%xd(Bd)|%xd(Bd)|%xd(Bd)|%xd(Bd)>

# Right side, power icon.
%V(201, 167, 39, 39, -)%Vf(_COLOR1)%Vb(_COLORBG)
%?bp<%xd(Cd)|%xd(Cc)>

# Right side, locked icon.
%V(201, 208, 39, 39, -)%Vf(_COLOR1)%Vb(_COLORBG)
%?mh<%xd(Cb)|%xd(Ca)>

# Right side, play/pause/stop/ff/rev indicator.
%V(201, 249, 39, 39, -)%Vf(_COLOR3)%Vb(_COLORBG)
%ar%?mp<%xd(Aa)|%xd(Ab)|%xd(Ac)|%xd(Ad)|%xd(Ae)>

