# Enable statusbar.
%we

# Font declarations.
%Fl(2, _FONT2)
%Fl(3, _FONT3)
%Fl(4, _FONT4)
%Fl(5, _FONT5)
%Fl(6, _FONT6)
%Fl(7, _FONT7)
%Fl(8, _FONT8)

# No background image.
%X(d)

# Left side, artist
%V(0, 0, 198, 16, 6)%Vf(_COLOR2)%Vb(_COLORBG)
%al%s%ia

# Left side, track title name
%V(0, 16, 198, 18, 7)%Vf(_COLOR4)%Vb(_COLORBG)
%al%s%?it<%it|%fn>

# Left side, composer
%V(0, 34, 198, 14, 3)%Vf(_COLOR2)%Vb(_COLORBG)
%al%s%ic

# Left side, album cover.
%V(0, 49, 198, 198, 3)%Vf(_COLOR2)%Vb(_COLORBG)
%Cl(0, 0, 198, 198, c, c)
%Cd

# Left side, album name.
%V(0, 252, 162, 16, 6)%Vf(_COLOR4)%Vb(_COLORBG)
%al%s%id

# Left side, year.
%V(162, 252, 36, 16, 6)%Vf(_COLOR4)%Vb(_COLORBG)
%ar%iy

# Left side, genre.
%V(0, 268, 140, 12, 3)%Vf(_COLOR2)%Vb(_COLORBG)
%al%s%ig

# Left side, playlist position.
%V(140, 268, 58, 14, 4)%Vf(_COLOR2)%Vb(_COLORBG)
%ar%pp/%pe

# All across, next track.
%V(0, 292, 240, 14, 3)%Vf(_COLOR2)%Vb(_COLORBG)
%s%al%?or(%Ia, %It, %Fn)<Next: %?Ia<%Ia%?It< - %It| - %Fn>|%?It<%It|%Fn>>>


########################################################################
# Progress bar
########################################################################
%?if(%pt,<,600)<%Vd(x)|%?if(%pt,<,3600)<%Vd(y)|%Vd(z)>>

### Less than 10 minutes.
# Left side, progress bar, current time
%Vl(x, 0, 308, 28, 12, 4)%Vf(_COLOR2)%Vb(_COLORBG)
%ar%pc

# Left side, progress bar, the progress bar itself.
%Vl(x, 28, 308, 184, 12, 4)%Vf(_COLOR3)%Vb(_COLORBG)
%pb(2, 2, 180, 8)

# Left side, progress bar, total time
%Vl(x, 212, 308, 28, 12, 4)%Vf(_COLOR2)%Vb(_COLORBG)
%al%pt

### 10 minutes or more, less than an hour.
# Left side, progress bar, current time
%Vl(y, 0, 308, 36, 12, 4)%Vf(_COLOR2)%Vb(_COLORBG)
%ar%pc

# Left side, progress bar, the progress bar itself.
%Vl(y, 36, 308, 126, 12, 4)%Vf(_COLOR3)%Vb(_COLORBG)
%pb(2, 2, 122, 8)

# Left side, progress bar, total time
%Vl(y, 162, 308, 36, 12, 4)%Vf(_COLOR2)%Vb(_COLORBG)
%al%pt

# More than an hour.
# Left side, progress bar, current time
%Vl(z, 0, 308, 46, 12, 4)%Vf(_COLOR2)%Vb(_COLORBG)
%ar%pc

# Left side, progress bar, the progress bar itself.
%Vl(z, 46, 308, 106, 12, 4)%Vf(_COLOR3)%Vb(_COLORBG)
%pb(2, 2, 102, 8)

# Left side, progress bar, total time
%Vl(z, 152, 308, 46, 12, 4)%Vf(_COLOR2)%Vb(_COLORBG)
%al%pt

