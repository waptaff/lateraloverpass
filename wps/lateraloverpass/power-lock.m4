<svg width="39" height="156">
	<defs>
		<!-- Common button box. -->
		<g id="box">
			<rect x="0"   y="0"   width="39" height="39" style="fill: _COLORBG;" />
			<rect x="0.5" y="0.5" width="38" height="38" rx="2" ry="2" style="stroke: _COLOR1; stroke-width: 1;
				fill: _COLORBG;" />
		</g>
	</defs>

	<!--
		Stupid M4 will not do two macro expansions for colors on
		the same line, because the replacement pattern contains
		a hash mark, which is interpreted as a comment.  That
		explains the weird line breaks inside style attributes.
	-->

	<!-- Disabled lock icon. -->
	<svg x="0" y="0">
		<use href="#box" x="0" y="0" />
		<rect x="15.5" y="11.5" width="8" height="10" rx="2.5" ry="2.5" style="stroke: _COLOR1; stroke-width: 3;
			fill: none;" />
		<rect x="11.5" y="17.5" width="16" height="10" rx="0.5" ry="0.5" style="stroke: _COLOR1; stroke-width: 1;
			fill: _COLOR1;" />
	</svg>

	<!-- Enabled lock icon. -->
	<svg x="0" y="39">
		<use href="#box" x="0" y="0" />
		<rect x="15.5" y="11.5" width="8" height="10" rx="2.5" ry="2.5" style="stroke: _COLOR3; stroke-width: 3;
			fill: none;" />
		<rect x="11.5" y="17.5" width="16" height="10" rx="0.5" ry="0.5" style="stroke: _COLOR3; stroke-width: 1;
			fill: _COLOR3;" />
	</svg>

	<!-- Disabled power icon. -->
	<svg x="0" y="78">
		<use href="#box" x="0" y="0" />
		<g style="stroke: _COLOR1; stroke-width: 3;
			fill: _COLORBG;">
			<path d="M 23.5 12.5 A 8 8 0 1 1 15.5 12.5" />
		</g>
		<rect x="19" y="11" width="1" height="6" style="stroke: _COLOR1; stroke-width: 2;
			fill=_COLOR1;" />
	</svg>

	<!-- Enabled power icon. -->
	<svg x="0" y="117">
		<use href="#box" x="0" y="0" />
		<g style="stroke: _COLOR3; stroke-width: 2;
			fill: _COLORBG;">
			<path d="M 23.5 12.5 A 8 8 0 1 1 15.5 12.5" />
		</g>
		<rect x="19" y="11" width="1" height="6" style="stroke: _COLOR3; stroke-width: 2;
			fill=_COLOR3;" />
	</svg>
</svg>
