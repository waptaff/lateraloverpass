<svg width="18" height="72">
	<defs>
		<!-- Common button box. -->
		<g id="box">
			<rect x="0" y="0" width="18" height="18" style="fill: _COLORBG;" />
			<rect x="0.5" y="0.5" rx="2" ry="2" width="17" height="17" style="stroke: _COLOR1; stroke-width: 1;
			fill: _COLORBG;" />
		</g>
	</defs>

	<!--
		Stupid M4 will not do two macro expansions for colors on
		the same line, because the replacement pattern contains
		a hash mark, which is interpreted as a comment.  That
		explains the weird line breaks inside style attributes.
	-->

	<!-- Disabled shuffle icon. -->
	<svg x="0" y="0">
		<use href="#box" x="0" y="0" />
		<rect x="3.5" y="3.5" width="11" height="11" rx="1.5" ry="1.5" style="stroke: _COLOR1; stroke-width: 1;
			fill: _COLOR1;" />
		<circle cx="6" cy="6" r="1" style="stroke: _COLORBG; stroke-width: 0.5;
			fill: _COLORBG;" />
		<circle cx="9" cy="9" r="1" style="stroke: _COLORBG; stroke-width: 0.5;
			fill: _COLORBG;" />
		<circle cx="12" cy="12" r="1" style="stroke: _COLORBG; stroke-width: 0.5;
			fill: _COLORBG;" />
	</svg>

	<!-- Enabled shuffle icon. -->
	<svg x="0" y="18">
		<use href="#box" x="0" y="0" />
		<rect x="3.5" y="3.5" width="11" height="11" rx="1.5" ry="1.5" style="stroke: _COLOR3; stroke-width: 1;
			fill: _COLOR3;" />
		<circle cx="6" cy="6" r="1" style="stroke: _COLORBG; stroke-width: 0.5;
			fill: _COLORBG;" />
		<circle cx="9" cy="9" r="1" style="stroke: _COLORBG; stroke-width: 0.5;
			fill: _COLORBG;" />
		<circle cx="12" cy="12" r="1" style="stroke: _COLORBG; stroke-width: 0.5;
			fill: _COLORBG;" />
	</svg>

	<!-- Disabled repeat icon. -->
	<svg x="0" y="36">
		<use href="#box" x="0" y="0" />
		<g style="stroke: _COLOR1; stroke-width: 2;
			fill: _COLORBG;">
			<path d="M 9 4 A 5 5 0 1 1 4 9" />
		</g>
		<rect x="9" y="3" width="5" height="0.5" style="stroke: _COLOR1; stroke-width: 0.5;
			fill: _COLOR1;" />
		<rect x="9" y="3" width="0.5" height="5" style="stroke: _COLOR1; stroke-width: 0.5;
			fill: _COLOR1;" />
	</svg>

	<!-- Enabled repeat icon. -->
	<svg x="0" y="54">
		<use href="#box" x="0" y="0" />
		<g style="stroke: _COLOR3; stroke-width: 2;
			fill: _COLORBG;">
			<path d="M 9 4 A 5 5 0 1 1 4 9" />
		</g>
		<rect x="9" y="3" width="5" height="0.5" style="stroke: _COLOR3; stroke-width: 0.5;
			fill: _COLOR3;" />
		<rect x="9" y="3" width="0.5" height="5" style="stroke: _COLOR3; stroke-width: 0.5;
			fill: _COLOR3;" />
	</svg>
</svg>
