<svg width="39" height="195">
	<defs>
		<!-- Common button box. -->
		<g id="box">
			<rect x="0"   y="0"   width="39" height="39" style="fill: _COLORBG;" />
			<rect x="0.5" y="0.5" width="38" height="38" rx="2" ry="2" style="stroke: _COLOR1; stroke-width: 1;
				fill: _COLORBG;" />
		</g>
	</defs>

	<!--
		Stupid M4 will not do two macro expansions for colors on
		the same line, because the replacement pattern contains
		a hash mark, which is interpreted as a comment.  That
		explains the weird line breaks inside style attributes.
	-->

	<!-- Stop icon. -->
	<svg x="0" y="0">
		<use href="#box" x="0" y="0" />
		<rect x="11.5" y="11.5" width="16"  height="16"  style="stroke: _COLOR3; stroke-width: 1;
			fill: _COLOR3;"  />
	</svg>

	<!-- Play icon. -->
	<svg x="0" y="39">
		<use href="#box" x="0" y="0" />
		<polygon points="11.5,11.5 11.5,27.5, 27.5,19.5" style="stroke: _COLOR3; stroke-width: 1;
			fill: _COLOR3;"  />
	</svg>

	<!-- Pause icon. -->
	<svg x="0" y="78">
		<use href="#box" x="0" y="0" />
		<rect x="11.5" y="11.5" width="5" height="16" style="stroke: _COLOR3; stroke-width: 1;
			fill: _COLOR3;"  />
		<rect x="22.5" y="11.5" width="5" height="16" style="stroke: _COLOR3; stroke-width: 1;
			fill: _COLOR3;"  />
	</svg>

	<!-- Fast-forward icon. -->
	<svg x="0" y="117">
		<use href="#box" x="0" y="0" />
		<polygon points="11.5,11.5 11.5,27.5, 19.5,19.5" style="stroke: _COLOR3; stroke-width: 1;
			fill: _COLOR3;"  />
		<polygon points="19.5,11.5 19.5,27.5, 27.5,19.5" style="stroke: _COLOR3; stroke-width: 1;
			fill: _COLOR3;"  />
	</svg>

	<!-- Rewind icon. -->
	<svg x="0" y="156">
		<use href="#box" x="0" y="0" />
		<polygon points="19.5,11.5 19.5,27.5, 11.5,19.5" style="stroke: _COLOR3; stroke-width: 1;
			fill: _COLOR3;"  />
		<polygon points="27.5,11.5 27.5,27.5, 19.5,19.5" style="stroke: _COLOR3; stroke-width: 1;
			fill: _COLOR3;"  />
	</svg>
</svg>
