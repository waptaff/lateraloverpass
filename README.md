# Lateral Overpass theme for Rockbox xDuoo X3ii

This repository stores the recipes to build the Lateral Overpass theme.

To build the theme, the following pretty standard tools are required:

	- GNU Make
	- GNU M4
	- ImageMagick
	- rsvg-convert
	- zip

## Building

As simple as `make`; that will create a `lateraloverpass.zip` file that
can be unzipped at the root of your SD card.


## Colors

The color palette can be adjusted at will by modifying the top of the
`settings.mk` file.  Icons will be also be tinted with the same color
palette, hence the need for ImageMagick's `convert` and `rsvg-convert`.

A single background color plus four foreground colors are defined:

Variable | Usage
-------- | -----------------------------------------------------------
`COLORBG`| Background.
`COLOR1` | Box outlines and dimmed icons.
`COLOR2` | Primary text.  Most text, plus bat/vol bars and values.
`COLOR3` | Primary highlight.  Time, date, active icons, progress bar.
`COLOR4` | Secondary highlight.  Track name, album title and year.


## Fonts

Fonts can also be adjusted inside `settings.mk`, but the layout will
_not_ accomodated by different font metrics/sizes.

Included Rockbox-compatible fonts were all converted from the very fine
and legible [Overpass font](https://overpassfont.org/), distributed
under the SIL Open Font License.


## Copyright and license.

Theme was written by Patrice Levesque.  Everything in this repository,
except the fonts, has been released to the public domain in 2021.


