fms: -
wps: /.rockbox/wps/lateraloverpass.wps
sbs: /.rockbox/wps/lateraloverpass.sbs
font: /.rockbox/fonts/18-overpass-bold.fnt
foreground color: _COLOR2
background color: _COLORBG
line selector start color: _COLOR1
line selector end color: _COLOR1
line selector text color: _COLOR4
list separator color: _COLOR4
statusbar: custom
