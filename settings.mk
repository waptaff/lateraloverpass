# Configuration directives.  See `README.md` for
# more information.

# Is this for the Rockbox simulator?
# The simulator does not have a clock.
# If set to 1, will hardcode date and time
# inside the theme files.
SIMULATOR = 0

# Dark theme
COLORBG = 000000
COLOR1  = 383E56
COLOR2  = C5D7BD
COLOR3  = FB743E
COLOR4  = FFFFFF

# Alternative light theme
# COLORBG = FAFAFA
# COLOR1  = E3E3E3
# COLOR2  = cb3737
# COLOR3  = ee6f57
# COLOR4  = 000000

FONT2 = 12-overpass-bold.fnt
FONT3 = 16-overpass-regular.fnt
FONT4 = 16-overpass-mono-regular.fnt
FONT5 = 18-overpass-mono-bold.fnt
FONT6 = 18-overpass-bold.fnt
FONT7 = 20-overpass-bold.fnt
FONT8 = 16-overpass-bold.fnt

