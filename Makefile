include settings.mk

.PHONY: clean default

default: lateraloverpass.zip

clean: # {{{
	@printf 'Cleaning up.\n'
	@rm \
		--force                            \
		lateraloverpass.zip                \
		themes/lateraloverpass.cfg         \
		wps/lateraloverpass.sbs            \
		wps/lateraloverpass.wps            \
		wps/lateraloverpass/icons.bmp      \
		wps/lateraloverpass/playstate.bmp  \
		wps/lateraloverpass/power-lock.bmp
# }}}
lateraloverpass.zip: themes/lateraloverpass.cfg wps/lateraloverpass.sbs wps/lateraloverpass.wps wps/lateraloverpass/icons.bmp wps/lateraloverpass/playstate.bmp wps/lateraloverpass/power-lock.bmp # {{{
	@printf 'Creating ZIP file.\n'
	$(eval TMP := $(shell mktemp -d))
	@install \
		-D                                         \
		--mode=0664                                \
		--target-directory=$(TMP)/.rockbox/themes/ \
		themes/lateraloverpass.cfg
	@install \
		-D                                      \
		--mode=0664                             \
		--target-directory=$(TMP)/.rockbox/wps/ \
		wps/lateraloverpass.sbs                 \
		wps/lateraloverpass.wps
	@install \
		-D                                                      \
		--mode=0664                                             \
		--target-directory=$(TMP)/.rockbox/wps/lateraloverpass/ \
		wps/lateraloverpass/icons.bmp                           \
		wps/lateraloverpass/playstate.bmp                       \
		wps/lateraloverpass/power-lock.bmp
	@install \
		-D                                        \
		--mode=0664                               \
		--target-directory=$(TMP)/.rockbox/fonts/ \
		fonts/*fnt
	@pushd $(TMP) > /dev/null; zip -rq9 lateraloverpass.zip .rockbox; popd > /dev/null; mv --force $(TMP)/lateraloverpass.zip .
	@rm -rf $(TMP)
# }}}
themes/lateraloverpass.cfg: themes/lateraloverpass.cfg.m4 settings.mk # {{{
	@printf 'Building theme configuration file.\n'
	@m4 \
		-D_COLORBG=$(COLORBG) \
		-D_COLOR1=$(COLOR1)   \
		-D_COLOR2=$(COLOR2)   \
		-D_COLOR3=$(COLOR3)   \
		-D_COLOR4=$(COLOR4)   \
		-D_COLOR5=$(COLOR5)   \
		$< > $@
# }}}
wps/lateraloverpass.sbs: wps/lateraloverpass.sbs.m4 wps/lateraloverpass/icons.bmp wps/lateraloverpass/playstate.bmp wps/lateraloverpass/power-lock.bmp settings.mk # {{{
	@printf 'Building theme statusbar file.\n'
	@m4 \
		-D_COLORBG=$(COLORBG)     \
		-D_COLOR1=$(COLOR1)       \
		-D_COLOR2=$(COLOR2)       \
		-D_COLOR3=$(COLOR3)       \
		-D_COLOR4=$(COLOR4)       \
		-D_COLOR5=$(COLOR5)       \
		-D_FONT2=$(FONT2)         \
		-D_FONT3=$(FONT3)         \
		-D_FONT4=$(FONT4)         \
		-D_FONT5=$(FONT5)         \
		-D_FONT6=$(FONT6)         \
		-D_FONT7=$(FONT7)         \
		-D_FONT8=$(FONT8)         \
		-D_SIMULATOR=$(SIMULATOR) \
		$< > $@
# }}}
wps/lateraloverpass.wps: wps/lateraloverpass.wps.m4 wps/lateraloverpass/icons.bmp wps/lateraloverpass/playstate.bmp wps/lateraloverpass/power-lock.bmp settings.mk # {{{
	@printf 'Building theme while playing screen file.\n'
	@m4 \
		-D_COLORBG=$(COLORBG)     \
		-D_COLOR1=$(COLOR1)       \
		-D_COLOR2=$(COLOR2)       \
		-D_COLOR3=$(COLOR3)       \
		-D_COLOR4=$(COLOR4)       \
		-D_COLOR5=$(COLOR5)       \
		-D_FONT2=$(FONT2)         \
		-D_FONT3=$(FONT3)         \
		-D_FONT4=$(FONT4)         \
		-D_FONT5=$(FONT5)         \
		-D_FONT6=$(FONT6)         \
		-D_FONT7=$(FONT7)         \
		-D_FONT8=$(FONT8)         \
		-D_SIMULATOR=$(SIMULATOR) \
		$< > $@
# }}}
wps/lateraloverpass/playstate.bmp: wps/lateraloverpass/playstate.m4 settings.mk # {{{
	@printf 'Building play state icons.\n'
	@m4 \
		-D_COLORBG=\#$(COLORBG) \
		-D_COLOR1=\#$(COLOR1)   \
		-D_COLOR2=\#$(COLOR2)   \
		-D_COLOR3=\#$(COLOR3)   \
		-D_COLOR4=\#$(COLOR4)   \
		-D_COLOR5=\#$(COLOR5)   \
		$< \
		| rsvg-convert -f png | convert - $@
# }}}
wps/lateraloverpass/power-lock.bmp: wps/lateraloverpass/power-lock.m4 settings.mk # {{{
	@printf 'Building power-lock icons.\n'
	@m4 \
		-D_COLORBG=\#$(COLORBG) \
		-D_COLOR1=\#$(COLOR1)   \
		-D_COLOR2=\#$(COLOR2)   \
		-D_COLOR3=\#$(COLOR3)   \
		-D_COLOR4=\#$(COLOR4)   \
		-D_COLOR5=\#$(COLOR5)   \
		$< \
		| rsvg-convert -f png | convert - $@
# }}}
wps/lateraloverpass/icons.bmp: wps/lateraloverpass/icons.m4 settings.mk # {{{
	@printf 'Building miscellaneous icons.\n'
	@m4 \
		-D_COLORBG=\#$(COLORBG) \
		-D_COLOR1=\#$(COLOR1)   \
		-D_COLOR2=\#$(COLOR2)   \
		-D_COLOR3=\#$(COLOR3)   \
		-D_COLOR4=\#$(COLOR4)   \
		-D_COLOR5=\#$(COLOR5)   \
		$< \
		| rsvg-convert -f png | convert - $@
# }}}
